import { Get, Post } from '../app/models/requests';

export class Examples {
  @Get('/hello_world')
  helloWorld(queryData: any): any {
    return { message: 'Hello world!', data: queryData };
  }

  @Post('/bye_world')
  byeWorld(params: any): any {
    return { message: 'Hello world!', data: params };
  }
}