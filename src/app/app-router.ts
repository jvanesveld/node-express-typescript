import express = require('express');
import { Route } from './models/route';

export class AppRouter {

  public routes: Route[] = [];
  private _expressRouter: express.Router = express.Router();

  public get expressRouter(): express.Router {
    if (this._expressRouter === undefined) {
      this._expressRouter = express.Router();
    }
    return this._expressRouter;
  }

  addRoutes(...routes: Route[]): void {
    this.routes = routes;
    this.apply();
  }

  addRoute(route: Route): void {
    this.routes.push(route);
    this.apply();
  }

  removeRoutes(...routes: string[]): void {
    this.routes = this.routes.filter(route => routes.indexOf(route.path) === -1);
    this.apply();
  }

  clear(): void {
    this.routes = [];
    this._expressRouter = express.Router();
  }

  private apply(): void {
    this.routes.map((route: Route) => route.bind(this._expressRouter));
  }
}
