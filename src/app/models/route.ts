import { RequestHandler, Router } from 'express';

export class Route {
  constructor(
    public path: string,
    private response: RequestHandler,
    public type = 'all'
  ) { }

  bind(router: Router): void {
    if (this.type in router) {
      router[this.type](this.path, this.response);
    }
  }
}
