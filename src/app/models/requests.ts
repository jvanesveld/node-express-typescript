import { RequestHandler } from 'express';
import { App } from '..';
import { Route } from './route';

export function Get(path: string): any {
  return (target, key) => {
    const response: RequestHandler = (req, res) => {
      target[key].bind(target);
      const result = target[key](req.query, res);
      if (!res.headersSent) {
        res.send(result);
      }
    };
    App.router.addRoute(
      new Route(
        path,
        response,
        'get'
      )
    );
    return target;
  };
}

export function Post(path: string): any {
  return (target, key) => {
    const response: RequestHandler = (req, res) => {
      target[key].bind(target);
      const result = target[key](req.body, res);
      if (!res.headersSent) {
        res.send(result);
      }
    };
    App.router.addRoute(
      new Route(
        path,
        response,
        'post'
      )
    );
    return target;
  };
}