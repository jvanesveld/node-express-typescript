import * as cors from 'cors';
import * as express from 'express';
import { AppRouter } from './app-router';

export class App {
  public static router: AppRouter = new AppRouter();

  private static _app;
  private static _port: number;

  private static get app(): any {
    if (this._app === undefined) {
      this._app = express();
      this._app.use(cors({ credentials: true, origin: true }));
      this._app.use(express.json());
      this._app.use((req, res, next) => {
        this.router.expressRouter(req, res, next);
      });
    }
    return this._app;
  }

  private static get port(): number {
    if (this._port === undefined) {
      const args = process.argv.slice(2);
      this._port = args[0] ? Number.parseInt(args[0]) : Number.parseInt(process.env.PORT) || 3000;
    }
    return this._port;
  }

  static listen(): void {
    this.app.listen(this.port, () => {
      // tslint:disable-next-line: no-console
      console.log(`Listening on port ${this.port}`);
    }).on('error', (err) => {
      // tslint:disable-next-line: no-console
      console.log(`Couldn't start listening on port ${this.port}`);
    });
  }
}
